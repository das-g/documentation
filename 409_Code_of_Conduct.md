# Citizens of Hereva Code of Conduct

![](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2018-11-28_coc_by-David-Revoy.jpg)

This document outlines the code of conduct for participating with the Pepper&Carrot web comic and related projects. 

Hereva is the world in which Pepper and Carrot live. This code of conduct uses examples and metaphors from Hereva to help lighten the spirit and tone of a code of conduct. Our intention is to keep the spirit and humor of Pepper&Carrot alive in our community and we hope that this code of conduct reflects that intention.

## Conduct

* Hereva brings together a myriad of diverse creatures and cultures. Dragon Cows fly overhead, while fierce Phandas lurk in the forests. Spider Admins create a literal web of connectivity, and uplifted ants build rockets to explore their environment. We welcome this diversity in our own community, and bring our own unique backgrounds and preferences to help build this world. We recognize that we are all unique and we respect and celebrate that uniqueness in each of us without diminishing the uniqueness of others. We are committed to providing a friendly, safe and welcoming environment for all.

* One of the main features of the Pepper&Carrot comic is how widely translated it is. This feature brings together many different languages and cultures together toward a common goal – to see Pepper&Carrot shared with as many people as possible. We welcome these languages and cultures and extend our community to all who provide their talents in translation and respect the languages and cultures of others working on the project.

* Pepper&Carrot is aimed at appealing to all ages with a primary focus toward younger readers. The use of overtly sexual language, nicknames, or imagery should be avoided in online communication related to the project.

* Please be kind and courteous. There's no need to be mean or rude. We are at our best when we are respectful to each other.

* Respect that people have differences of opinion and that every design or implementation choice carries a trade-off and can cascade into numerous fixes and time costs. There is seldom a right answer.

* We will argue from time to time. Please keep unstructured critique to a minimum. If you have solid ideas you want to experiment with, make a fork or branch and see how it works.

* We will exclude you from interaction if you insult, demean or harass anyone. That is not welcome behavior. We interpret the term "harassment" as including the definition in the "Unacceptable Behavior" section of the [Citizen Code of Conduct][citizen_code_of_conduct] ([archive link][citizen_code_of_conduct_archive]); if you have any lack of clarity about what might be included in that concept, please read their definition. In particular, we don't tolerate behavior that excludes people in socially or ethnically marginalized groups.

* Private harassment is also unacceptable. No matter who you are, if you feel you have been or are being harassed or made uncomfortable by a community member, please contact one of the channel ops or any of the Pepper&Carrot maintainer team immediately. Whether you're a regular contributor or a newcomer, we care about making this community a safe place for you. We have your back.

* Likewise any spamming, trolling, flaming, baiting or other attention-stealing behavior is not welcome.

* The use of curse words (profanity) is tolerated, but please keep it to a minimum. A good barometer of acceptable language would be if you would want that language to be displayed in a Pepper&Carrot comic, or an article about Pepper&Carrot.

## Moderation
These are the principles and policies for upholding our community's standards of conduct. If you feel that a thread or channel discussion needs moderation, please contact the Pepper&Carrot moderators listed at the end of this document(referred to as "Moderators" in this text).

* Remarks that violate the Citizens of Hereva code of conduct, including hateful, hurtful, oppressive, or exclusionary remarks, are not allowed. (Cursing is tolerated, but never targeting another user, and never in a hateful manner.)

* Remarks that moderators find inappropriate, whether listed in the code of conduct or not, are also not allowed.

* Moderators will first respond to such remarks with a warning.

* If the warning is unheeded, the user will be "kicked," i.e., temporarily removed from the communication channel to cool off.

* If the user comes back and continues to make trouble, they will be banned, i.e., indefinitely excluded.

* Moderators may choose at their discretion to un-ban the user if it was a first offense and they offer the offended party a genuine apology.

* If a moderator bans someone and you think it was unjustified, please take it up with that moderator, or with a different moderator, in private. Complaints about bans in-channel are not allowed.

* Moderators are held to a higher standard than other community members. If a moderator creates an inappropriate situation, they should expect less leeway than others.

In the Pepper&Carrot community we strive to go the extra step to look out for each other. Don't just aim to be technically unimpeachable, try to be your best self. In particular, avoid flirting with offensive or sensitive issues, particularly if they're off-topic; this all too often leads to unnecessary fights, hurt feelings, and damaged trust. Worse, it can drive people away from the community entirely.

If someone takes issue with something you said or did, resist the urge to be defensive. Just stop doing what it was they complained about, acknowledge that the issue occurred, and offer an apology if needed. Even if you feel you were misinterpreted or unfairly accused, chances are good there was something you could've communicated better — remember that it's your responsibility to help make your fellow Citizens of Hereva comfortable. We are all here first and foremost because we want to talk about and collaborate on the amazing Pepper&Carrot comics from David Revoy. Everyone wants to get along with each other. You will find that people will be eager to assume good intent and forgive as long as you earn their trust.

The enforcement policies listed above apply to all official Pepper&Carrot venues; including official communication channels (#pepper&carrot on Libera, Telegram, Mattermost, Framateam, etc.); GitLab repositories hosted in the Framagit [peppercarrot][peppercarrot_framagit] group; and all forums under [peppercarrot.com][peppercarrot]. For other projects adopting the Citizens of Hereva Code of Conduct, please contact the maintainers of those projects for enforcement. If you wish to use this code of conduct for your own project, consider explicitly mentioning your moderation policy or making a copy with your own moderation policy so as to avoid confusion.

## Moderators

### Craig Maloney (aka cmaloney)

Craig Maloney has been a fan of computers since he first learned about them from the World Book Encyclopedia (remember those?). By day he programs computers and by night he also programs computers. He is the maintainer of the Pepper&Carrot Wiki where he works to document and expand the world of Hereva for others to use in their project, including a long-delayed RPG that he's working on. He is the host of [Open Metalcast](http://openmetalcast.com) and is one of the board members for the [Michigan!/usr/group](http://mug.org). Learn more at his [site](http://decafbad.net).

**Contact:** [Private message on IRC](http://web.libera.chat/#pepper&carrot) or [craig@decafbad.net](mailto:craig@decafbad.net).

---

### David Revoy (aka deevad)

David Revoy is a French artist born in 1981 and the author of Pepper&Carrot. He is passionate about drawing, painting, cats, computers, GNU/Linux, Free/Libre and Open-source Culture, Internet, old school RPG video games, old manga and anime, traditional art, Japanese culture, fantasy… He also likes to share tutorials and making-ofs on his [blog](https://www.davidrevoy.com).

**Contact:** [Private message on IRC](http://web.libera.chat/#pepper&carrot) or [info@davidrevoy.com](mailto:info@davidrevoy.com).

---

### Valvin (aka valvin)

Valvin was born in France in 1981. He also speaks English but makes a lot of mistakes. Passionate about Free/Opensource software and culture, he has a particular interest in Internet decentralization, privacy and [Framasoft](https://framasoft.org)'s campaigns. He likes drawing but still has a lot to learn. Contributions on P&C are about beta-reading and being a member of the French translators. [His blog (fr)](https://blog.valvin.fr)

**Contact:** [Private message on IRC](http://web.libera.chat/#pepper&carrot) or `@valvin:matrix.org` on [Matrix](https://riot.im/app/#/room/#pepper&carrot:matrix.org) [valvin1@protonmail.com](mailto:valvin1@protonmail.com).

---

## Attribution
This document is adapted from [The Rust Code of Conduct][rust_code_of_conduct], and from the [Contributor Covenant][contributor_covenant], version 1.4. It is our intention to bring the best parts of those documents into an agreement that benefits everyone who contributes to Pepper&Carrot.

[moderators]: https://www.peppercarrot.com/en/static14/documentation&page=420_Moderators
[citizen_code_of_conduct]: http://citizencodeofconduct.org/#unacceptable-behavior
[citizen_code_of_conduct_archive]: https://web.archive.org/web/20181116012721/http://citizencodeofconduct.org/#unacceptable-behavior
[peppercarrot_framagit]: https://framagit.org/peppercarrot
[peppercarrot]: https://www.peppercarrot.com
[rust_code_of_conduct]: https://www.rust-lang.org/en-US/conduct.html
[contributor_covenant]: https://www.contributor-covenant.org/version/1/4/code-of-conduct.html
