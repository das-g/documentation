# Best practices for attribution

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_00_thumbnailb.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_00_thumbnailb.jpg)

For my webcomic Pepper&Carrot, I want to give people the right to **share, use, create, build** and even **make money** upon the artworks, stories and universe I've created. (an exception: please do not make NFTs with my art, [it infringes my moral right](https://www.davidrevoy.com/article864/dream-cats-nfts-don-t-buy-them)).  

For this, I'm publishing my content under a specific license: [The Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/ "The Creative Commons Attribution 4.0"). It's a **very permissive license**: you just need to **write the "attribution"** in order to reuse the work. An attribution is an appropriate credit to the author's name or nicknames (artists, correctors, translators involved in the creation of the media you want to use). 

With an attribution for the Creative Commons Attribution 4.0 license (let's shorten it to CC-By) **you also need to mention the license** of the media (and provide a link if you can) and indicate if you made changes. You may do all of this in any reasonable manner, but not in any way that suggests the authors endorse you or your use. So, let see with a practical example what the **best practices to write attribution for Pepper&Carrot** are:

## The basic one:

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_01_simple.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_01_simple.jpg)

That's the basic case: you just want to reuse the media as it is. You'll need to write the title of the artwork, my name and the CC-By license.

*Note: Bonus points if you create an active link on [the title to the source](https://www.peppercarrot.com/en/static6/sources&page=other "the title to the source") and to the [CC-By 4.0 full license](https://creativecommons.org/licenses/by/4.0/ "CC-By 4.0 full license").*

**Full:**

    "Artwork title" by David Revoy, licensed under Creative Commons Attribution 4.0.

**Short:**

    "Artwork title" by D.Revoy, CC-By

**Mini:**

    Art:D.Revoy/ᶜᶜᴮᵞ

## In case of modification:

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_02_derivation.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_02_derivation.jpg)

If you modified the original artwork, you need to tell your audience
how. You just need to indicate "what you did" so the audience will
understand "who did what".

**Full:**

    A derivative by MyNameHere of "Artwork title" by David Revoy
    licensed under Creative Commons Attribution 4.0. Change made: Crop and color balanced.

**Short:**

    A derivative (crop/color) by MyNameHere of "Artwork title" by D.Revoy, CC-By

**Mini:**

    (Crop/color)MyNameHere/Art:D.Revoy/ᶜᶜᴮᵞ


## In case of modification and relicensing

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_03_relicensing.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_03_relicensing.jpg)


The license of Pepper&Carrot is so permissive that it allows you to use another license than the CC-By. For this, you **have to write the license** you want to use (*compatible CC-By-Sa, CC-By-Nc, CC-By-Nd or even Copyright, but not compatible Public Domain/CC-0*) and **keep indicating** that the original artwork is released as CC-By. Relicensing your own derivation will not affect the CC-By original file.

**Full:**

    A derivation Copyrighted by MyNameHere of "Artwork title linked to the original page" by David Revoy
    originally licensed under Creative Commons Attribution 4.0. Change made: Blue cat is better, added a pencil.

**Short:**

    A Copyrighted derivative (blue version/pencil) by MyNameHere of "Artwork title" by D.Revoy, CC-By

**Mini:**

    (blue/pencil)©MyNameHere/Art:ᶜᶜᴮᵞD.Revoy


## Comic publishing:

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_04_comic.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_04_comic.jpg)
*The second page of the Glénat published comic, with the whole list of attributions*

Comic stories have **more contributors**: proofreaders, script doctors, translators, scenarists, etc. and they are all based on the Hereva universe (the collaborative CC-By world described on the [Wiki of Pepper&Carrot](https://www.peppercarrot.com/static8/wiki "Wiki of Pepper&Carrot")). To know the full attribution of an episode, check in two places: the last page(s) of the webcomic online, and the source page of the episode in [Sources &gt; Webcomic](https://www.peppercarrot.com/en/static6/sources "Sources > Webcomic").

**Full:** *(for episode 21 in English)*

    Art & Scenario: David Revoy - Translation: Alex Gryson
    Brainstorming: Craig Maloney, Quiralta, Nicolas Artance, Talime, Valvin.
    Dialog Improvements: Craig Maloney, Jookia, Nicolas Artance and Valvin.
    Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney.
    Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
    License: Creative Commons Attribution 4.0

**Minified:**

    CC-By/Art&Scenario: D.Revoy/Translation:A.Gryson/Brainstorming:C.Maloney,Quiralta,N.Artance,Talime,Valvin
    Dialog Improvements:C.Maloney,Jookia,N.Artance,Valvin/Universe of Hereva:David Revoy with contributions:C.Maloney
    Corrections: W.Sonke,Moini,Hali,CGand,Alex Gryson.

## Movies/animation and video game derivations

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_05_movie.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_05_movie.jpg)
*4 screens: the start of the game/movie with a simple general attribution and the end credits with detailed attribution.*

Modern video games, animations and movies include cinematic introductions (even if they are just a fading between slides of company logos and a title; I consider it as cinematic.) **The first screens** in the introduction are often a place of choice to insert the general credit. The full and detailed attribution can be found at the end of the movie/game; or in a submenu of the media (game start/chapter).

**Intro screen:**

    Based on the webcomic Pepper&Carrot by David Revoy

**Full credits later (end credits):**

    Based on the webcomic Pepper&Carrot by David Revoy.
    https://www.peppercarrot.com
    Licensed under the Creative Commons Attribution 4.0.
    https://creativecommons.org/licenses/by/4.0/
    Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney.
    Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.

## Role-playing games and board games

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_06_boardgame.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_06_boardgame.jpg)

If you decide to publish a printed role-playing game or a board game about Pepper&Carrot, you'll find yourself in a situation a bit similar to the movie/video game above. But the general credit will go on the cover, and the detailed credit in the printed rules or on the back of the box. Don't forget to attribute the end product in the online store listing too.

**Box cover:**

    Based on the webcomic Pepper&Carrot by David Revoy

**Later (rules, or back of the box):**

    Based on the webcomic Pepper&Carrot by David Revoy.
    https://www.peppercarrot.com
    Licensed under the Creative Commons Attribution 4.0.
    https://creativecommons.org/licenses/by/4.0/
    Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney.
    Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.

## Crowdfunding campaign

If you launch a crowdfunding campaign (Kickstarter/Indiegogo/etc...) around Pepper&Carrot, just attribute the media you use in the campaign correctly and add an important disclaimer to let your audience know some basic information. Especially **"where the money goes"**. In fact, when re-using Pepper&Carrot artworks, universe, etc; a big part of your audience will imagine that they are funding me as an independent artist. You need a disclaimer to tell that in fact, it's your project they fund, not the artworks you use...

**The disclaimer:**

    DISCLAIMER:
    This campaign is collecting funds exclusively for my <MyNameHere> work on the production of <MyProject>.
    David Revoy, the original author of the web-comic Pepper&Carrot is not involved in the production process
    and does not receive/shares any revenues collected here.
    The name of David Revoy on images are specified for attribution purposes but not as sign of affiliation.
    You can support his work and webcomic through his Patreon page:
    https://www.patreon.com/davidrevoy

## Software and source code

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_07_sources.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_07_sources.jpg)

A **README.md** at the root of your project is usually a good place to write information about attributions and credits. If your project is bigger, you might want a separate CREDITS file.

**Markdown:**

    Based on the webcomic [Pepper&Carrot](https://www.peppercarrot.com) by David Revoy
    licensed under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
    Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney.
    Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.

## Objects/merchandising/commercial goodies

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_08_object.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_08_object.jpg)

Clothes, figurines, toys and other real-life objects also require a proper attribution on the product itself, on the box/packaging and in the online store listing:

**Product label and online store listing**

    A derivative product designed by MyName/CompanyHere, based on the webcomic Pepper&Carrot CC-BY David Revoy

**Short:**

    Product design: MyName/CompanyHere, based on Pepper&Carrot/CC-BY D.Revoy

**Minified:**

    MyName/CompanyHere, based on Pepper&Carrot ᶜᶜᴮᵞ

## Fan art

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_09_fanart.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_09_fanart.jpg)

If you do fan art, a good practice is to write the attributions/information directly on the picture. This information survives longer than if you write it in the description. *Note: On social networks, using the hashtag \#peppercarrot is not an attribution. But it's fun :-)*

**Fan art attribution:**

    "My title", a fan art by MyNameHere of Pepper&Carrot by D.Revoy, CC-By


## Fan fiction

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_10_fanfiction.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_10_fanfiction.jpg)

If you write fan fiction, the best place is to attribute it in the header. So your reader will directly know what they are reading.

**Fan fiction attribution:**

    "My fiction title" by MyNameHere, based on the CC-By webcomic Pepper&Carrot by David Revoy

## Forum, community, channel

[![](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_11_community.jpg)](https://www.peppercarrot.com/data/documentation/medias/img/2017-03_goodpractise_ccby_11_community.jpg)

External communities, forums and social network pages are helping a lot with growing the community. If you manage or created a page like this on your favorite network, you'll need to attribute it this way:

**For the header:**

    Unofficial community based on the universe of the CC-By webcomic Pepper&Carrot by David Revoy

**In the "About" page, longer version with more information:**

    Based on the webcomic [Pepper&Carrot](https://www.peppercarrot.com) by David Revoy
    licensed under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
    Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney.
    Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.

## End notes

I hope this guide will help you go ahead with your project by providing all the necessary information without waiting for a private authorization or dealing with the long process of setting up a contract. If you want more information, read the Creative Commons documentation. This page was freely inspired by their wiki page "[Best practices for attribution](https://wiki.creativecommons.org/wiki/best_practices_for_attribution "Best practices for attribution")". And if you have any doubts, leave a comment below or better: email me info@davidrevoy.com :-)
