---
title: Kotlin Workshop — Exercises
author:
  - Christian Mäder
institute:
  - nxt Engineering GmbH
date: 2019-09-06
---

# Chapter 2

## Exercise 1: Install Kotlin

For very simple examples, there's the [Kotlin Playground][kotlin_playground], which only requires a modern web browser.

But this will not be enough for the code we're going to write below. So you will need to install Kotlin to your computer.

### Download and Install Kotlin

There are multiple ways to get Kotlin:

* [Standalone Kotlin][kotlin_cli]:
  Compile and Run the program from a command line prompt
* [IntelliJ IDEA][kotlin_idea]:
  Write the code in a modern IDE that already ships with Kotlin.
  Use either the Community Edition or the Ultimate Edition.
* [Eclipse][kotlin_eclipse]:
  Use the Eclipse plugin for Kotlin to write and compile code within that IDE.

[kotlin_eclipse]: https://kotlinlang.org/docs/tutorials/getting-started-eclipse.html
[kotlin_idea]: https://kotlinlang.org/docs/tutorials/getting-started.html
[kotlin_cli]: https://kotlinlang.org/docs/tutorials/command-line.html

### Compile and Run on the CLI

If you work on the CLI, you can compile Kotlin code using `kotlinc filename.kt`.
To run the code, use `kotlin MainClassName`.

Hint for Java Developers: `kotlin` is going to invoke `java` in the background.
But the `kotlin` command automatically adds it's own standard library classes to the classpath so that you don't have to do that.

Consider the following example code:

```{.kotlin .numberLines}
// Filename: Hello.kt
fun main() {
  println("Hello World")
}
```

Compile and run this piece of code like this:

```bash
$ kotlinc Hello.kt
$ kotlin HelloKt
Hello World
```

### Compile and Run in IntelliJ

Working in an IDE has many advantages, and IntelliJ is a particular helpful IDE.
To get started with Kotlin in IntelliJ IDEA I recommend you to read through the official [_Getting Started with IntelliJ IDEA_][kotlin_idea] guide.

Try to get the following code to run:

```{.kotlin .numberLines}
// Filename: Hello.kt
fun main() {
  println("Hello IntelliJ IDEA")
}
```

### Compile and Run in Eclipse

Eclipse was one of the first powerful and free Java IDEs on the market.
To get started with Kotlin in Eclipse I recommend you to read through the official [_Getting Started with Eclipse_][kotlin_eclipse] guide.

Try to get the following code to run:

```{.kotlin .numberLines}
// Filename: Hello.kt
fun main() {
  println("Hello Eclipse")
}
```

## Exercise 2: Rock Paper Scissors

The goal is to program a small Rock Paper Scissors game.

Just in case you forgot the rules, here they are:

> 1. Either player decides between rock, paper or scissors.
> 2. At the same time, both reveal their decision.
> 3. Rock beats scissors, scissors beat paper, paper beats rock.

### Part 1: Input / Output

Because printing something to the terminal (i.e. `stdout`) – and reading something back from it (i.e. reading from `stdin`) – is a really common use-case, Kotlin has predefined functions for it:

```{.kotlin .numberLines}
// Filename: InOut.kt
fun main() {
  print("What's your name? ")
  val name = readLine()
  println("Hello, $name. How are you?")
}
```

The first goal is now to write a program that prints the following:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: s
You've chosen s.
```

For that, create a new file called `Game.kt`.
Copy the code from the `InOut.kt` program from further above and change it until it works as shown in the snippet right above.

#### Solution

One possible solution is the this piece of code:

```{.kotlin .numberLines}
// Game.kt
fun main() {
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userChoice = readLine()
  println("You've chosen $userChoice.")
}
```

### Part 2: Parsing Input

By printing "`You've chosen s.`" in the example above, we're telling the user that the program understood the input.
This is already nice feedback!
But the sentence "_You've chosen s._" does not read as well as "_You've chosen scissors._", does it?
Also, when the user inputs anything else than `r`, `p` or `s`, we would like to tell the user this was not the expected input.

One way to achieve this is to use the [`when`](https://kotlinlang.org/docs/reference/control-flow.html#when-expression) expression.
We will use it to map an input value to a different output value.
See the following example:

```{.kotlin .numberLines}
// Filename: GoodOrBad.kt
fun main() {
    print("How are you? Type 'g' for good or 'b' for bad: ")
    val stateOfMind = readLine()

    val response = when(stateOfMind) {
        "g" -> "Glad to hear you're doing good!"
        "b" -> "Sorry to hear, I hope live turns to the better soon!"
        else -> "Sorry, I don't understand what you mean."
    }

    println(response)
}
```

Here we use the `when` expression to map the input `"g"` (for _good_) and `"b"` (for _bad_) to an appropriate response.
There is also an `else` part, which is mandatory most of the time.
This answer will be chosen whenever the input of the user was not one of the expected `"g"` or `"b"`.

Using the example above, try to extend your code in a way, such that `"r"` is mapped to `"rock"`, `"p"` is mapped to `"paper"` and `"s"` is mapped to `"scissors"`.
And when the user entered something else than `"r"`, `"p"` or `"s"`, then map it to `"unknown"`.

When the user interacts with your problem, it should behave like that:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: s
You've chosen scissors.
```

Can you do that?

#### Solution

Your code will eventually look similar to this:

```{.kotlin .numberLines}
// Game.kt
fun main() {
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")
}
```

[when]: https://kotlinlang.org/docs/reference/control-flow.html#when-expression

### Part 3: Computer

The game does not work if there is no counterpart.
We're going to add a computer player next.

The computer player will have to make a choice as well.
(Either before the player enters it's choice or after.)

It will face the same choices like the player, i.e. playing _rock_, _paper_ or _scissors_.
We can put these three choices into an array and pick one randomly.

In Kotlin, arrays can be created using the built-in function [`arrayOf`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/array-of.html).
We can then call the `random()` function on the array to get one element of it at random.

The following example shows how to use `arrayOf()` and the `random()` call:

```{.kotlin .numberLines}
// Array.kt
fun main() {
  val greetings = arrayOf(
    "Hello", "Grüezi", "Guten Tag", "Buon giorno", "God dag",
    "Daginn", "Bonjour", "Bom dia", "Goedendag", "Buenos Días", "Olá!")
  val greeting = greetings.random()
  println("$greeting!")
}
```

Using the example above, try to add code to your game, such that the computer draws one of `"rock"`, `"paper"` or `"scissors"`.
That program's output should look something like this afterwards:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: s
You've chosen scissors.
The computer has chosen rock.
```

#### Solution

```{.kotlin .numberLines}
// Game.kt
fun main() {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")
}
```

### Part 4: Referee

Only few games have no winner (or at least no looser).
_Rock, Paper, Scissors_ is no different.
We therefore need some piece of code to decide who wins and who looses, the player or the computer.

We already know the `when` syntax, and it will become handy in this case as well.
Previously, we've used the syntax to assign a value based on an input:

```{.kotlin .numberLines}
// PreviouslyInKotlin.kt
fun main() {
  print("Enter 'y' for Yes or 'n' for No: ")
  val input = readLine()

  val output =
    when(input) {
      "y" -> "Yes, Yes, Yes!"
      "n" -> "Oh no! Why!?"
      else -> "Sorry, I didn't hear you. Could you say that again?"
    }

  println(output)
}
```

If I make the first example for our referee code, can you do the rest?

```{.kotlin .numberLines}
// Game.kt
fun main() {
  // player draws
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  // computer draws
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  // referee
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> "Paper beats rock, the computer wins."
      "scissors" -> "Rock beats scissors, the player wins."
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    "paper" -> ...
    "scissors" -> ...
    else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
  }

  println(result)
}
```

> _Note:_ If the `// player draws`, `//computer draws` or `// referee` lines confuse you, these are called _comments_.
> Everything that follows two slashes (`//`) is ignored and will not be part of the final program.

In the code above, replace the ellipsis (`...`) with your code.
The output of the program should afterwards like this:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
```

#### Solution

```{.kotlin .numberLines}
// Game.kt
fun main() {
  // player draws
  println("Hello.")
  print("Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  // computer draws
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  // referee
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> "Paper beats rock, the computer wins."
      "scissors" -> "Rock beats scissors, the player wins."
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    "paper" -> when(computerChoice) {
      "rock" -> "Paper beats rock, the player wins."
      "paper" -> "It's a tie"
      "scissors" -> "Scissors beat paper, the computer wins."
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    "scissors" -> when(computerChoice) {
      "rock" -> "Rock beats scissors, the computer wins."
      "paper" -> "Scissors beat paper, the player wins."
      "scissors" -> "It's a tie"
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
  }

  println(result)
}
```

### Part 5: Loops

Wouldn't it be nice, if we could play the game over and over again?
Without having it to restart each time?

We can achieve this using loops.
See the following program.
It asks the user the guess what the computer thinks until it receives the right answer:

```{.kotlin .numberLines}
// While.kt
fun main() {
    println("I will choose one of 'a', 'b' or 'c'.")
    val computerChoice = arrayOf("a", "b", "c").random()

    print("Can you guess what I've chosen? ")
    var userChoice = readLine()

    while (userChoice != computerChoice) {
        print("Can you guess what I've chosen? ")
        userChoice = readLine()
    }

    println("Yes, I've thought of '$computerChoice'.")
}
```

There are two important new concepts in the code above:

1. The `while` loop will run the code it contains as long as the condition holds true.
   I.e. in the case above, as long as `userChoice` is not equal to `computerChoice`, it will ask the player to try again.
2. Note the `var` – instead of `val` – in the code above.
   The difference is that `val` stands for _value_ and it can't be changed after it is assigned for the first time.
   The `var` however stands for `variable` and this means that it can be changed later on.

Compare the following code snippet to the one above:

```{.kotlin .numberLines}
// DoWhile.kt
fun main() {
    println("I will choose one of 'a', 'b' or 'c'.")
    val computerChoice = arrayOf("a", "b", "c").random()

    do {
        print("Can you guess what I've chosen? ")
        val userChoice = readLine()
    } while (userChoice != computerChoice)

    println("Yes, I've thought of '$computerChoice'.")
}
```

What has changed?

In the second example the `do ... while` loop was introduced.
It is almost the same as the `while ` loop.
But it runs the code it contains at least once.
After the code is run, it checks whether the given condition (`userChoice != computerChoice`) is true.
If it is, it will run the code it contains again.
It will then re-evaluate the condition and decide, whether to run the code again.
This repeats until the condition becomes false.
In the case above, the code in the loop is repeated until the `userChoice` and the `computerChoice` are the same value.

With the knowledge about the `do ... while` loop, are you able extend your game in such a way, that the user is asked at the end of the program whether it should start again?

The output afterwards may look like this:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
Would you like to play again? Type 'y' for yes and 'n' for no. y
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
Would you like to play again? Type 'y' for yes and 'n' for no. n
```

> _Hint:_ Use the methods you already know to ask the user a question and store the answer.
> Then use the answer in the condition of the loop.

#### Solution

```{.kotlin .numberLines}
// Game.kt
fun main() {
  do {
    // player draws
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> "rock"
        "p" -> "paper"
        "s" -> "scissors"
        else -> "unknown"
      }
    println("You've chosen $userChoice.")

    // computer draws
    val choices = arrayOf("rock", "paper", "scissors")
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    // referee
    val result = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> "Paper beats rock, the computer wins."
        "scissors" -> "Rock beats scissors, the player wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "paper" -> when(computerChoice) {
        "rock" -> "Paper beats rock, the player wins."
        "paper" -> "It's a tie"
        "scissors" -> "Scissors beat paper, the computer wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "scissors" -> when(computerChoice) {
        "rock" -> "Rock beats scissors, the computer wins."
        "paper" -> "Scissors beat paper, the player wins."
        "scissors" -> "It's a tie"
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }

    println(result)

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

> _Hint:_ Note the `do {` on the third line!

### Part 6: Functions

The code of the game is now already pretty long.
That's why we're now going to break up the code in smaller pieces.
This can be done by functions.

You've already seen one function, the `main` function.
It's the part of the code that says `fun main()`.

But we can also create other functions.
Take the following code for example:

```{.kotlin .numberLines}
// DiceRoll.kt
fun main() {
  do {
    // roll dice
    val diceRoll = arrayOf(1,2,3,4,5,6).random()
    println("The dice shows $diceRoll.")

    // One more time?
    print("Roll the dice again? Yes [y] or No [n]? ")
  } while (readLine() == "y")
}
```

The program simulates a dice.
It takes a random number, starting with 1 and up to 6, and prints it out.
It then asks the user wether it should "roll the dice" again.

Below is essentially the same code, but the part where it simulates the _dice rolling_ is now in a function `diceRoll()`.
This function is then called in the `main()` function:

```{.kotlin .numberLines}
// DiceRoll.kt
fun rollDice() { // new function
  val diceRoll = arrayOf(1,2,3,4,5,6).random()
  println("The dice shows $diceRoll.")
}

fun main() {
  do {
    rollDice() // call the existing code

    // One more time?
    print("Roll the dice again? Yes [y] or No [n]? ")
  } while (readLine() == "y")
}
```

> _Hint:_ If you use comments to group sections of code, these comments – more often than not – are a hint to you that this part of the code could also be a function.
> You can see that what previously was a comment `// roll dice` is not a function called `rollDice()`.

Some functions are just called.
But we've already seen at least one function that also returns a value:
The `readLine()` function.
We can write such functions ourselves as well.

In the code above, the `// One more time?` section would be a good example.
It asks a questions and reads a value from a user.
This value is then used to make a decision.

The code below shows how writing your own function works in Kotlin:

```{.kotlin .numberLines}
// DiceRoll.kt
fun rollDice() {
    val diceRoll = arrayOf(1,2,3,4,5,6).random()
    println("The dice shows $diceRoll.")
}

fun oneMoreTime(): Boolean {
    print("Roll the dice again? Yes [y] or No [n]? ")
    val answer = readLine()
    val playOneMoreTime = (answer == "y")
    return playOneMoreTime
}

fun main() {
    do {
        rollDice()
    } while (oneMoreTime())
}
```

The code has been changed again.
The question (whether to roll the dice again) is now it's own function.
This function contains the code to read the answer.
It then compares the answer with the answer expected to continue.
The result of the comparison is returned.
The `do {...} while(...)` uses that returned value for it's decision whether another iteration is requested, or if the user is done for now.

It's time for you to apply that to the _Rock, Paper, Scissors_ game.
Try to introduce two new functions:

1. A function `fun playerDraw(): String {...}`.
   It should contain all the commands that are required so that the _player_ can enter the choice whether to play rock, paper or scissors.
2. A function `fun computerDraw(): String {...}`.
   It should contain all the command that are required so that the _computer_ can make it's choice.

> _Bonus:_ If you figured out how to write the functions above, try to also extract a `oneMoreTime()` function, like in the example above.

#### Solution

```{.kotlin .numberLines}
// Game.kt
fun playerDraw(): String {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  return userChoice
}

fun computerDraw(): String {
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  return computerChoice
}

fun main() {
  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    // referee
    val result = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> "Paper beats rock, the computer wins."
        "scissors" -> "Rock beats scissors, the player wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "paper" -> when(computerChoice) {
        "rock" -> "Paper beats rock, the player wins."
        "paper" -> "It's a tie"
        "scissors" -> "Scissors beat paper, the computer wins."
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      "scissors" -> when(computerChoice) {
        "rock" -> "Rock beats scissors, the computer wins."
        "paper" -> "Scissors beat paper, the player wins."
        "scissors" -> "It's a tie"
        else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
      }
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }

    println(result)

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

### Part 7: Functions with Arguments

It would also be nice to extract the `// referee` part into it's own function.
To do that, we have to learn how to pass something to a function.

```{.kotlin .numberLines}
// WordAnalyzer.kt
fun main() {
  println("Please enter two words:")
  val first = readLine().orEmpty()
  val second = readLine().orEmpty()

  // compare
  val firstLen = first.length
  val secondLen = second.length
  println("The first word is $firstLen characters long.")
  println("The second word is $secondLen characters long.")
  if (firstLen < secondLen) {
    val difference = secondLen - firstLen
    println("'$second' is $difference characters longer than '$first'.")
  } else {
    val difference = firstLen - secondLen
    println("'$first' is $difference characters longer than '$second'.")
  }
}
```

All the lines after `// compare` logically belong together.
Therefore it's a good candidate to extract into a function.
The problem is:
That code depends on the two variables `first` and `second`.

In Kotlin values can be passed to functions.
These values, that are passed to a function, are called _arguments of the function_.

See the following example:

```{.kotlin .numberLines}
// WordAnalyzerFun.kt
fun main() {
  println("Please enter two words:")
  val first = readLine().orEmpty()
  val second = readLine().orEmpty()

  compare(first, second)
}

fun compare(first: String, second: String) {
  val firstLen = first.length
  val secondLen = second.length
  println("The first word is $firstLen characters long.")
  println("The second word is $secondLen characters long.")
  if (firstLen < secondLen) {
    val difference = secondLen - firstLen
    println("'$second' is $difference characters longer than '$first'.")
  } else {
    val difference = firstLen - secondLen
    println("'$first' is $difference characters longer than '$second'.")
  }
}
```

Look at the function definition:

```{.kotlin .numberLines}
fun compare(first: String, second: String) {
  // ...
}
```

It reads "the function called _compare_ has the arguments _first_ (of type _String_) and _second_ (of type String)".
Within the function body, `first` and `second` can be used like any other value.

The call to the function is slightly different:

```{.kotlin .numberLines}
fun main() {
  println("Please enter two words:")
  val first = readLine().orEmpty()
  val second = readLine().orEmpty()

  compare(first, second)
}
```

The values `first` and `second` are passed to the function `compare` as arguments.

Now, can you apply the same principle to the game?
Can you extract the `// referee` section of the code into it's own function?

#### Solution {#solution7}

```{.kotlin .numberLines}
// Game.kt
fun playerDraw(): String {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  return userChoice
}

fun computerDraw(): String {
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  return computerChoice
}

fun referee(userChoice: String, computerChoice: String) {
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> "Paper beats rock, the computer wins."
      "scissors" -> "Rock beats scissors, the player wins."
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    "paper" -> when(computerChoice) {
      "rock" -> "Paper beats rock, the player wins."
      "paper" -> "It's a tie"
      "scissors" -> "Scissors beat paper, the computer wins."
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    "scissors" -> when(computerChoice) {
      "rock" -> "Rock beats scissors, the computer wins."
      "paper" -> "Scissors beat paper, the player wins."
      "scissors" -> "It's a tie"
      else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
    }
    else -> "It's unclear if '$userChoice' beats '$computerChoice', or not."
  }

  println(result)
}

fun main() {
  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

### Part 8: Classes

When playing a game, wouldn't it be nice to keep count of the score?
Like, how many times the player won, and how many times the computer?
Let's add this to our game!

First, we need a way to keep track of the score, i.e. a scoreboard.
Before we're going to add this into our game, we'll see how it could work in a simplified case.

The first thing we're going to learn is to create a class.
A class can be thought of as a group of information that logically belongs together.
In our case, we're going to build a scoreboard class:

```{.kotlin .numberLines}
// Scoreboard.kt
class Scoreboard {
  var computerScore = 0
  var playerScore = 0
}
```

In the code above, we've created a `Scoreboard` class.
This class can keep track of two variables:
The `computerScore` and the `playerScore`.
When we speak of variables that belong to a class, we call them _fields_.

A class can have additional functionality.
For the scoreboard, it would be nice to print the current score.
We can add a function to the class to do that.
We call such a function a _method_.

```{.kotlin .numberLines}
// Scoreboard.kt
class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore.")
  }
}
```

Do you recognize the pattern?
The `fun printScore() { ... }` is a function just like the functions we've written above.
But it is declared in the context of the `class Scoreboard`.
That's what makes it what we call a _method_.

In contrast to the functions we used before, the _method_ can access all the _fields_ of the class, to which it belongs.
That's why the `printScore()` method can directly use `$computerScore`.

Until now you were not able to test this piece of code.
Let's change that:

```{.kotlin .numberLines}
// Scoreboard.kt
fun main() {
  val gameScore = Scoreboard()
  gameScore.printScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }
}
```

The `main()` function makes use of the `Scoreboard`.
Instead of assigning a number (e.g. `5`) or a string (e.g. `"hello"`) to the value `gameScore`, we're assigning a new instance of our class.

And instead of calling the `printScore()` function directly, we say that we want to call the `printScore()` method of `gameScore`.

If you run this code, the output should be like this:

```
The computer won 0 times and the player 0 times.
```

This scoreboard is not very useful as it is, because there is no way to increase the scores.
We will create two new methods to increase the scores:
One method to increase the `computerScore` and one to increase the `playerScore`:

```{.kotlin .numberLines}
// Scoreboard.kt
fun main() {
  val gameScore = Scoreboard()
  gameScore.printScore()
  gameScore.increaseComputerScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}
```

Now, for this to be "fun", let's increase the scores and see what that does.
We'll call the `increaseComputerScore()` method and the `increasePlayerMethod()` in the `main()` function.
And in between we'll call `printScore()` to see if it worked:

```{.kotlin .numberLines}
// Scoreboard.kt
fun main() {
  val gameScore = Scoreboard()
  gameScore.printScore()
  gameScore.increaseComputerScore()
  gameScore.printScore()
  gameScore.increasePlayerScore()
  gameScore.printScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}
```

The output will look like this:

```
The computer won 0 times and the player 0 times.
The computer won 1 times and the player 0 times.
The computer won 1 times and the player 1 times.
```

The nice thing about the class is that variables are tied to the _instance_.
By calling `val scoreboard = Scoreboard()`, we've created a new instance of the `Scoreboard` class and assigned it to the value called `scoreboard`.

We can create a second scoreboard as well:


```{.kotlin .numberLines}
// Scoreboard2.kt
fun main() {
  val gameScore1 = Scoreboard()
  val gameScore2 = Scoreboard()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore1.increaseComputerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore2.increaseComputerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore1.increasePlayerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()

  gameScore2.increasePlayerScore()

  print("Board 1: ")
  gameScore1.printScore()
  print("Board 2: ")
  gameScore2.printScore()
}

class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}
```

If you run this code, it's output will look like this:

```
Board 1: The computer won 0 times and the player 0 times.
Board 2: The computer won 0 times and the player 0 times.
Board 1: The computer won 1 times and the player 0 times.
Board 2: The computer won 0 times and the player 0 times.
Board 1: The computer won 1 times and the player 0 times.
Board 2: The computer won 1 times and the player 0 times.
Board 1: The computer won 1 times and the player 1 times.
Board 2: The computer won 1 times and the player 0 times.
Board 1: The computer won 1 times and the player 1 times.
Board 2: The computer won 1 times and the player 1 times.
```

As you can see, the `gameScore1` and `gameScore2` are independent of each other.
This is one of the main advantages for using classes:
Values and functionality that belongs together is grouped and can be re-used throughout our source code again and again.

It's now time to use this wisdom and integrate a scoreboard into our game.
If you're brave, go ahead, and try it yourself!
Otherwise, we'll do it step by step based on the [solution of part 6](#solution7).

The first thing you need to do is to copy the code for the `Scoreboard` class into you code:

```{.kotlin .numberLines}
// Game.kt
class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

fun playerDraw(): String {
// ... rest of the game ...
```

The next step will be to adjust the `main()` function, where you'll need to create a new instance of the `Scoreboard` class:

```{.kotlin .numberLines}
// Game.kt

// ... a lot of existing code here ...

fun main() {
  val gameScore = Scoreboard()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

You need to create the scoreboard before the `do {` line.
This is because the scoreboard should keep the score between all the rounds which the player plays.
But if it were after the `do {` line, the scores would always start at _0 : 0_ at the beginning of each round.

Next, you'll add a line which prints the score.
Add it after the `referee(...)` function, because that is the time where we know who has won this round:

```{.kotlin .numberLines}
// Game.kt

// ... a lot of existing code here ...

fun main() {
  val gameScore = Scoreboard()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    gameScore.printScore()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

When you run the game now, it will show a score after each round:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
The computer won 0 times and the player 0 times.
Would you like to play again? Type 'y' for yes and 'n' for no. y
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
The computer won 0 times and the player 0 times.
Would you like to play again? Type 'y' for yes and 'n' for no. n
```

But it is not the correct score yet.
That's why you will have to adjust the `referee(...)` function.
When we want to be able to adjust the `gameScore` in that function, we need to know about it.
That's why you'll need to add a new argument to the function:

```{.kotlin .numberLines}
// before
fun referee(userChoice: String, computerChoice: String) {
  // ... code ...
}

// after
fun referee(userChoice: String, computerChoice: String, gameScore: Scoreboard) {
  // ... code ...
}
```

The next step is to adjust the `where` part.
Whenever there is a winner, we'll need to increment the score for the winner.
This can be done like this:

```{.kotlin .numberLines}
// Game.kt

// ... some other code ...

fun referee(userChoice: String, computerChoice: String, gameScore: Scoreboard) {
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> {
        gameScore.increaseComputerScore()
        "Paper beats rock, the computer wins."
      }
      "scissors" -> {
        gameScore.increasePlayerScore()
        "Rock beats scissors, the player wins."
      }
      else -> throw RuntimeException("Unknown choice.")
    }
    "paper" -> when(computerChoice) {
      "rock" -> {
        gameScore.increasePlayerScore()
        "Paper beats rock, the player wins."
      }
      "paper" -> "It's a tie"
      "scissors" -> {
        gameScore.increaseComputerScore()
        "Scissors beat paper, the computer wins."
      }
      else -> throw RuntimeException("Unknown choice.")
    }
    "scissors" -> when(computerChoice) {
      "rock" -> {
        gameScore.increaseComputerScore()
        "Rock beats scissors, the computer wins."
      }
      "paper" -> {
        gameScore.increasePlayerScore()
        "Scissors beat paper, the player wins."
      }
      "scissors" -> "It's a tie"
      else -> throw RuntimeException("Unknown choice.")
    }
    else -> throw RuntimeException("Unknown choice.")
  }

  println(result)
}

// ... more code ...
```

It is important that you add the lines `gameScore.increasePlayerScore()` and `gameScore.increaseComputerScore()` before the lines with the text about who has won!

If you've combined this in the right way, your output should now look like this:

```
Hello.
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
The computer won 0 times and the player 1 times.
Would you like to play again? Type 'y' for yes and 'n' for no. y
Please choose between Rock [r], Paper [p] or Scissors [s]: p
You've chosen paper.
The computer has chosen rock.
Paper beats rock, the player wins.
The computer won 0 times and the player 2 times.
Would you like to play again? Type 'y' for yes and 'n' for no. n
```

If you code does not work like this, compare it with the following solution and see if you can spot your error.

#### Solution

```{.kotlin .numberLines}
// Game.kt
class Scoreboard {
  var computerScore = 0
  var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

fun playerDraw(): String {
  print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
  val userCommand = readLine()
  val userChoice =
    when (userCommand) {
      "r" -> "rock"
      "p" -> "paper"
      "s" -> "scissors"
      else -> "unknown"
    }
  println("You've chosen $userChoice.")

  return userChoice
}

fun computerDraw(): String {
  val choices = arrayOf("rock", "paper", "scissors")
  val computerChoice = choices.random()
  println("The computer has chosen $computerChoice.")

  return computerChoice
}

fun referee(userChoice: String, computerChoice: String, gameScore: Scoreboard) {
  val result = when(userChoice) {
    "rock" -> when(computerChoice) {
      "rock" -> "It's a tie"
      "paper" -> {
        gameScore.increaseComputerScore()
        "Paper beats rock, the computer wins."
      }
      "scissors" -> {
        gameScore.increasePlayerScore()
        "Rock beats scissors, the player wins."
      }
    }
    "paper" -> when(computerChoice) {
      "rock" -> {
        gameScore.increasePlayerScore()
        "Paper beats rock, the player wins."
      }
      "paper" -> "It's a tie"
      "scissors" -> {
        gameScore.increaseComputerScore()
        "Scissors beat paper, the computer wins."
      }
    }
    "scissors" -> when(computerChoice) {
      "rock" -> {
        gameScore.increaseComputerScore()
        "Rock beats scissors, the computer wins."
      }
      "paper" -> {
        gameScore.increasePlayerScore()
        "Scissors beat paper, the player wins."
      }
      "scissors" -> "It's a tie"
    }
  }

  println(result)
}

fun main() {
  val gameScore = Scoreboard()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    gameScore.printScore()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

### Part 9: Visibility

In the program above, the basic logic of our game is in the `main()` function.
This is not something we like very much in programming.
Because if we would want to add other functionality to our game (e.g. allowing the player to choose a name), we would have to add it to the `main()` function as well.
Even though it has nothing to do with *how* the flow of the game works.

In a programming language like Kotlin there is the concept of _classes_ to group data and functionality – that logically belongs together – together.
You've already learned this concept in the previous part, where we've created a `Scoreboard` class:
It contains only the data and logic related to the scoreboard.

As a next step in our game, together, we're going to extract all the functionality in the `main()` method, that is related to how the game is played, into a `Game` class.

So, the first step will be to create a new (empty) `Game` class:

> **Tip:** Create this new class right above the `main()` function.
> This will make it easier to copy-and-paste code between the two.

```{.kotlin .numberLines}
// Game.kt

// ... here comes all the other code ...

class Game {

}

fun main() {
  // ... here goes all the code of the main() function ...
}
```

Now we have to identify the pieces that belong to the game and move them into the class one by one, ideally without breaking our game after each step.

Therefore, as a first step, let's change the `main()` function a little:

```{.kotlin .numberLines}
// Game.kt

// ... here's all the other code ...

class Game {

}

fun main() {
  val game = Game()
  val gameScore = Scoreboard()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice, gameScore)

    gameScore.printScore()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

Did you spot the change?
I've just added a new value `val Game` and initialized it.

Now, we may recognize that the `gameScore` always belongs to a certain game.
Because if we say "we start a fresh game", it should start with a fresh game score.

So as a next step, we'll transfer the `gameScore` into the `Game` class:

```{.kotlin .numberLines}
// Game.kt

// ... the rest of the code is here ..

class Game {
  val score = Scoreboard()
}

fun main() {
  val game = Game()

  do {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice, game.score)

    game.score.printScore()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

I've also renamed the `gameScore` to `score`.
The reason is subtle:
Because the `score` belongs now to the `Game` class, we already know it's the game's score.
So the variable name does not need to repeat this information and our code will get a tiny bit simpler to read.

In order to pass the score of the game to the `referee()` function, we now have to get the `score` from the `game`.
This can be done with `game.score`.
It is the same concept as we've learned further above when we called the `printScore()`, `increaseComputerScore()` and `increasePlayerScore()` functions on the `Scoreboard` class.

The next step – in our mission to bring all the game-related code together – is to move all the code, that is in the `do {...} while(...)` loop.
All of the code, though?
I argue not.
Because I believe it is not the task of the `Game` to ask the player whether a new round should be played.

Applying these changes leads to the following code:

```{.kotlin .numberLines}
// Game.kt

// ... the rest of the code is here ..

class Game {
  val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice, score)

    score.printScore()
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

Besides cut-and-pasting the code, there is one more subtle change to be made:
Changing `game.score` to just `score`.
The reason is that – within the boundaries of the `Game` class – we can access the fields directly.
We've previously seen this in the `Scoreboard` class, when we increased the scores.

Now, when just looking at the `main()` function:
Do you agree, that it's much simpler like this to understand what the `main()` function does?

Still, there is another thing that bothers me with the code we have now:
The functions `referee()`, `playerDraw()` and `computerDraw()` are directly related to how the game works, but they are not yet within the boundary of the `Game` class.
So they _could_ still be called in the `main()` function:

```{.kotlin .numberLines}
// Game.kt

// ... all other code ...

fun main() {
  val game = Game()

  do {
    val notUseful = playerDraw()     // nothing stops us from doing this
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

See the line where I wrote `// nothing stops us from doing this` (on line 9)?
This line does – logically – not make a lot of sense, but to the computer, it's perfectly valid code.
It will just do as it's told and will therefore ask the player to make a choice (and then ignore this first choice).
And then, when `game.playARound()` is called, the player is asked to make a choice again.

In order to avoid this possibility altogether, we'll move (aka. cut-and-paste) the functions `referee()`, `playerDraw()` and `computerDraw()` within the boundaries of the `Game` class:

```{.kotlin .numberLines}
// Game.kt
class Scoreboard {
  // ... previous code of Scoreboard ...
}

class Game {
  val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice, score)

    score.printScore()
  }

  fun playerDraw(): String {
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> "rock"
        "p" -> "paper"
        "s" -> "scissors"
        else -> "unknown"
      }
    println("You've chosen $userChoice.")

    return userChoice
  }

  fun computerDraw(): String {
    val choices = arrayOf("rock", "paper", "scissors")
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }

  fun referee(userChoice: String, computerChoice: String, gameScore: Scoreboard) {
    val result = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> {
          gameScore.increaseComputerScore()
          "Paper beats rock, the computer wins."
        }
        "scissors" -> {
          gameScore.increasePlayerScore()
          "Rock beats scissors, the player wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "paper" -> when(computerChoice) {
        "rock" -> {
          gameScore.increasePlayerScore()
          "Paper beats rock, the player wins."
        }
        "paper" -> "It's a tie"
        "scissors" -> {
          gameScore.increaseComputerScore()
          "Scissors beat paper, the computer wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "scissors" -> when(computerChoice) {
        "rock" -> {
          gameScore.increaseComputerScore()
          "Rock beats scissors, the computer wins."
        }
        "paper" -> {
          gameScore.increasePlayerScore()
          "Scissors beat paper, the player wins."
        }
        "scissors" -> "It's a tie"
        else -> throw RuntimeException("Unknown choice.")
      }
      else -> throw RuntimeException("Unknown choice.")
    }

    println(result)
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

> **Hint:** If you look closely, you see that I've change the `referee()` function slightly:
> Because it is now within the context of the game, it has direct access to the `score` field.
> So there is no need to pass the current score as function argument.
> Therefore I have removed the `gameScore` argument from the argument list of the `referee` function and replaced any occurrence of `gameScore` with just `score`.

In the function `main()` it's not possible anymore to just call the `playerDraw()` function.
But it's still possible to call `game.playerDraw()`:

```{.kotlin .numberLines}
// Game.kt

// ... all other code ...

fun main() {
  val game = Game()

  do {
    val notUseful = game.playerDraw()     // nothing stops us from doing this
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

To prevent such code a concept called `visibility` was added to classes.
For every _field_ and every _function_ we can tell the compiler that it should be hidden to _the outside_, i.e. that only code within the boundary of a class can access that _field_ or _function_.
This can be done with the `private` keyword.

Let's look at the following simple example code.
It's purpose is to count points for two players called A and B:

```{.kotlin .numberLines}
// SimpleScoreBoard.kt
class SimpleScoreBoard {
  var countA = 0
  var countB = 0

  fun print() = println("The score is $countA vs $countB.")
}

fun main() {
  val scores = SimpleScoreBoard()
  do {
    print("Who has won? A or B? ")
    val choice = readLine()
    when (choice) {
      "A", "a" -> scores.countA = scores.countA + 1
      "B", "b" -> scores.countB = scores.countB - 1
      else -> println("Unknown choice '$choice'.")
    }
    scores.print()

    print("Again? Type 'y' for yes: ")
    val again = readLine()
  } while (again == "y")
}
```

There is a small error in the program.
Can you spot it?
Maybe you can after looking at the output of the program:

```
Who has won? A or B? A
The score is 1 vs 0.
Again? Type 'y' for yes: y
Who has won? A or B? B
The score is 1 vs -1.
Again? Type 'y' for yes: n
```

The program has a mistake at the end of line 16:
There is a `-` instead of a `+`.

Such things easily happen when code, which is not related to the actual class, directly modifies data of said class.
In this case, the `main()` function directly modified data of the `SimpleScoreBoard` class.

A better implementation of the `SimpleScoreBoard` is the following:

```{.kotlin .numberLines}
// PrivateScoreBoard.kt
class PrivateScoreBoard {
  private var countA = 0
  private var countB = 0

  fun increaseCountA() {
    countA = countA + 1
  }

  fun increaseCountB() {
    countB = countB + 1
  }

  fun print() = println("The score is $countA vs $countB.")
}

fun main() {
  val scores = PrivateScoreBoard()
  do {
    print("Who has won? A or B? ")
    val choice = readLine()
    when (choice) {
      "A", "a" -> scores.increaseCountA()
      "B", "b" -> scores.increaseCountB()
      else -> println("Unknown choice '$choice'.")
    }
    scores.print()

    print("Again? Type 'y' for yes: ")
    val again = readLine()
  } while (again == "y")
}
```

In this second example, the variables `countA` and `countB` can not be directly modified anymore, because they are made `private`.
`private` means, that only code within the boundaries of the class can read and write the variable.
Code from outside of the class can no longer read or write the variables directly.

Btw, the same is also possible for the methods of a class:
They can be made `private` and then only code, that is within the boundaries of the class, can call that method.
Code from outside the class can't.

Let's apply this to our _Rock, Paper, Scissors_ game.
We'll have to think about all the _fields_ and _methods_ of our two classes, `Scoreboard` and `Game`:

* In `class Scoreboard`:
  * _Should `var computerScore` be private?_
    Yes, it should!
    Because no code is supposed to change the value other than the code in `fun increaseComputerScore()`.
  * _Should `var playerScore` be private?_
    Yes, it should!
    Because no code is supposed to change the value other than the code in `fun increasePlayerScore()`.
  * _Should `fun printScore()` be private?_
    No, it should not.
    Because if it were private, then there would be no way to print the current score.
    It would forever be buried in the `Scoreboard` class.
  * _Should `fun increaseComputerScore()` be private?_
    No, it should not.
    Because if it were private, then it would not be possible to advance the score for the computer player.
  * _Should `fun increasePlayerScore()` be private?_
    No, it should not.
    Because if it were private, then it would not be possible to advance the score for the player.
* In `class Game`:
  * _Should `var score` be private?_
    Yes, it should!
    Because only code which is part of the `Game` class should be allowed to modify the score.
  * _Should `fun playARound()` be private?_
    No, it should not.
    Because if it where, there would be no way to play (another) round of the game.
  * _Should `fun playerDraw()` be private?_
    Yes, it should!
    Because it's the job of the `playARound()` method to coordinate the game.
    There is no need for other code to call this method.
  * _Should `fun computerDraw()` be private?_
    Yes, it should!
    Because it's the job of the `playARound()` method to coordinate the game.
    Again, there is no need for other code to call this method.
  * _Should `fun referee()` be private?_
    Yes, it should, for the same reason:
    It's the job of the `playARound()` method to coordinate the game.
    And again, there is no need for other code to call this method.

Now that we've discussed every _field_ and _method_ of our two classes `Scoreboard` and `Game`, can you apply the change to make the relevant _fields_ and _methods_ private?

#### Solution

```{.kotlin .numberLines}
// Game.kt
class Scoreboard {
  private var computerScore = 0
  private var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

class Game {
  private val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    score.printScore()
  }

  private fun playerDraw(): String {
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> "rock"
        "p" -> "paper"
        "s" -> "scissors"
        else -> "unknown"
      }
    println("You've chosen $userChoice.")

    return userChoice
  }

  private fun computerDraw(): String {
    val choices = arrayOf("rock", "paper", "scissors")
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }

  private fun referee(userChoice: String, computerChoice: String) {
    val result = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> "It's a tie"
        "paper" -> {
          score.increaseComputerScore()
          "Paper beats rock, the computer wins."
        }
        "scissors" -> {
          score.increasePlayerScore()
          "Rock beats scissors, the player wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "paper" -> when(computerChoice) {
        "rock" -> {
          score.increasePlayerScore()
          "Paper beats rock, the player wins."
        }
        "paper" -> "It's a tie"
        "scissors" -> {
          score.increaseComputerScore()
          "Scissors beat paper, the computer wins."
        }
        else -> throw RuntimeException("Unknown choice.")
      }
      "scissors" -> when(computerChoice) {
        "rock" -> {
          score.increaseComputerScore()
          "Rock beats scissors, the computer wins."
        }
        "paper" -> {
          score.increasePlayerScore()
          "Scissors beat paper, the player wins."
        }
        "scissors" -> "It's a tie"
        else -> throw RuntimeException("Unknown choice.")
      }
      else -> throw RuntimeException("Unknown choice.")
    }

    println(result)
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

### Part 10: Reduce Mental Load

When reading and writing code you have to keep a lot of information in your brain.
Therefore, when writing a program, we always prefer code that is easy to read.
In our program, `Game.kt`, we have one particular complicated part: `private fun referee(...)`.

Why this part?
There are a few symptoms:

- The method is more than 40 lines long.
  Even on a big screen the method hardly fits as a whole.
- A lot of the code looks redundant.
  That's because we repeat similar code for each of the game's allowed moves, i.e. _Rock, Paper, Scissors_.
  For a person not familiar with the code, it is not immediately clear, what all the nested `when` conditions do.
- We compare a lot of string values.
  Sometimes this is unavoidable.
  In this case there is a better solution (which are about to learn).

That's why we'll improve the `referee(...)` method next.
But first we are going to learn about enumeration classes, or _enum_ for short.

Enums are usually used when there is only a fixed number of options.
For example the months in a year:
Every year has exactly twelve months, January to December.
Or the days in a week, which are always Monday, Tuesday, Wednesday, Thursday, Friday, Saturday and Sunday.
(At least in the Gregorian calendar.)

Let's see how such an enum looks like in code:

```{.kotlin .numberLines}
// Weekday.kt
enum class Weekday {
  Monday, Tuesday,
  Wednesday, Thursday,
  Friday, Saturday,
  Sunday
}

fun main() {
  println(Weekday.values().joinToString(", "))

  var monday = Weekday.Monday
  println("Monday is '$monday'.")

  val weekday = Weekday.values().random()
  println("It's '$weekday'.")

  when(Weekday.Monday == weekday) {
    true -> println("What!? Already Monday?!")
    false -> println("Indeed, it's not yet Monday.")
  }

  val whatPeopleMightSay = when(weekday) {
    Weekday.Monday -> "It's the first day of the week."
    Weekday.Tuesday -> "It's already the second weekday."
    Weekday.Wednesday -> "Only two days of work left."
    Weekday.Thursday -> "Soon it's weekend. Just one more day."
    Weekday.Friday -> "Weekend is almos here. Ready to party?"
    Weekday.Saturday -> "Hangover?"
    Weekday.Sunday -> "You better relax, tomorrow's Monday again!"
  }
  println(whatPeopleMightSay)

  val isItWeekendYet = when(weekday) {
    Weekday.Saturday, Weekday.Sunday -> true
    else -> false
  }
  println("Is it weekend yet? $isItWeekendYet")
}
```

The `main()` function shows how to use such an enum.
As you can see on line 12 using a certain value of the enum is very simple:
`EnumName.ValueName`.
That's the enum's name and then the value you like to use.
To know whether a variable is of a certain enum-value the equals operator (`==`) can be used.

Now that we know the basics about Enumerations, let's apply this to the `fun referee(...)` in our Kotlin game, `Game.kt`.
In that method, I see two distinct places to make use of `enum` classes.
We'll do the less obvious together and I'll leave the other up to you.

Currently, the `fun referee(...)` basically consists of one magic `when (...) {...}` construct.
It has three purposes:

- Figure out which choice beats which.
- Increase the score for the winner.
- Stich-together a message that is shown to the player.

If we want to reduce the mental load for this part of the code, we'll need to separate out at least one of those purposes.

I want to separate out the decision making, i.e. the part that decides which choice beats which.
For that, I'm going to add the following enum right before the `fun referee(...)`:

```{.kotlin .numberLines}
// Game.kt

// ... existing code ...

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: String, computerChoice: String) {
    // ... existing code ...
  }

// ... the rest of the existing code ...
```

In the next step, I'll add a new `when`-clause at the beginning of the `fun referee(...)` method:

```{.kotlin .numberLines}
// Game.kt

// ... existing code ...

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: String, computerChoice: String) {
    val winner = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> Winner.NoOne
        "paper" -> Winner.Computer
        "scissors" -> Winner.Player
        else -> throw RuntimeException("Unknown choice.")
      }
      "paper" -> when(computerChoice) {
        "rock" -> Winner.Player
        "paper" -> Winner.NoOne
        "scissors" -> Winner.Computer
        else -> throw RuntimeException("Unknown choice.")
      }
      "scissors" -> when(computerChoice) {
        "rock" -> Winner.Computer
        "paper" -> Winner.Player
        "scissors" -> Winner.NoOne
        else -> throw RuntimeException("Unknown choice.")
      }
      else -> throw RuntimeException("Unknown choice.")
    }

    val result = when(userChoice) {
      // ... existing code ...
    }

    println(result)
  }

// ... the rest of the existing code ...
```

The sole purpose of the new code part is to figure out who wins this round.
Now that it is know who wins, it's possible to simplify the rest of the `fun referee(...)` code:

```{.kotlin .numberLines}
// Game.kt

// ... existing code ...

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: String, computerChoice: String) {
    val winner = when(userChoice) {
      "rock" -> when(computerChoice) {
        "rock" -> Winner.NoOne
        "paper" -> Winner.Computer
        "scissors" -> Winner.Player
        else -> throw RuntimeException("Unknown choice.")
      }
      "paper" -> when(computerChoice) {
        "rock" -> Winner.Player
        "paper" -> Winner.NoOne
        "scissors" -> Winner.Computer
        else -> throw RuntimeException("Unknown choice.")
      }
      "scissors" -> when(computerChoice) {
        "rock" -> Winner.Computer
        "paper" -> Winner.Player
        "scissors" -> Winner.NoOne
        else -> throw RuntimeException("Unknown choice.")
      }
      else -> throw RuntimeException("Unknown choice.")
    }

    val result = when(userChoice) {
      // ... existing code ...
    }

    val result = when(winner) {
      Winner.NoOne -> "It's a tie."
      Winner.Player -> {
        score.increasePlayerScore()
        "${userChoice.capitalize()} beats $computerChoice, the player wins."
      }
      Winner.Computer -> {
        score.increaseComputerScore()
        "${computerChoice.capitalize()} beats $userChoice, the computer wins."
      }
    }

    println(result)
  }

// ... the rest of the existing code ...
```

The part of the code, which renders the message to the user and increases the score counters, is now a lot simpler.
It's immediately clear what happens if the player won, if it's a tie or if the computer won.

But I'm not done yet:
The code in `fun referee(...)` is still too long for my taste.
Fortunately, now that the long `when`-clause has simply one job – determining a winner – we can extract some of it's functionality to other methods:

```{.kotlin .numberLines}
// Game.kt

// ... existing code ...

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: String, computerChoice: String) {
    val winner = when (userChoice) {
      "rock" -> rockGiven(computerChoice)
      "paper" -> paperGiven(computerChoice)
      "scissors" -> scissorsGiven(computerChoice)
      else -> throw RuntimeException("Unknown choice.")
    }

    val result = when (winner) {
      Winner.NoOne -> "It's a tie."
      Winner.Player -> {
        score.increasePlayerScore()
        "${userChoice.capitalize()} beats $computerChoice, the player wins."
      }
      Winner.Computer -> {
        score.increaseComputerScore()
        "${computerChoice.capitalize()} beats $userChoice, the computer wins."
      }
    }

    println(result)
  }

  private fun rockGiven(computerChoice: String): Winner {
    return when (computerChoice) {
      "rock" -> Winner.NoOne
      "paper" -> Winner.Computer
      "scissors" -> Winner.Player
      else -> throw RuntimeException("Unknown choice.")
    }
  }

  private fun paperGiven(computerChoice: String): Winner {
    return when (computerChoice) {
      "rock" -> Winner.Player
      "paper" -> Winner.NoOne
      "scissors" -> Winner.Computer
      else -> throw RuntimeException("Unknown choice.")
    }
  }

  private fun scissorsGiven(computerChoice: String): Winner {
    return when (computerChoice) {
      "rock" -> Winner.Computer
      "paper" -> Winner.Player
      "scissors" -> Winner.NoOne
      else -> throw RuntimeException("Unknown choice.")
    }
  }

// ... the rest of the existing code ...
```

Granted, the overall length of the code increased because of the extra methods `fun rockGiven(...)`, `fun paperGiven(...)` and `fun scissorsGiven(...)`.
But if you look at just the code of `fun referee(...)`, I believe that it became a whole lot simpler to grasp.
And because all the `fun xxxGiven(...)` functions work almost the same, you'd have to study just one of them to understand all of them.

If you haven't applied the code changes up to here, please do that.
Because now it will be your turn to introduce another `enum`.

Your task is to eliminate the need for the `"rock"`, `"paper"`, `"scissors"` strings:
Make an `enum class Choice {...}` with the options `Rock`, `Paper`, `Scissors`.
Than try to replace all the before-mentioned `String` values with it.
I.e. if there's a `"rock"` in the code, it would look like `Choice.Rock` after you're done with the changes.

> _Tip 1:_
> To make a random choice in the `computerDraw()` method, you can replace `arrayOf("rock", "paper", "scissors")` with `Choice.values()`.

> _Tip 2:_
> After you replaced all the `"rock"`, `"paper"` and `"scissors"` strings, you can simplify some of the `when (...) {...}` statements.
> Because when using an `enum` value with the `when (...) {...}` construct, we don't need an `else` part, if we cover all the options of the enum, i.e. `Choice.Rock`, `Choice.Paper` and `Choice.Scissors`.

#### Solution

```{.kotlin .numberLines}
// Game.kt
class Scoreboard {
  private var computerScore = 0
  private var playerScore = 0

  fun printScore() {
    println("The computer won $computerScore times and the player $playerScore times.")
  }

  fun increaseComputerScore() {
    computerScore = computerScore + 1
  }

  fun increasePlayerScore() {
    playerScore = playerScore + 1
  }
}

class Game {
  private val score = Scoreboard()

  fun playARound() {
    val userChoice = playerDraw()
    val computerChoice = computerDraw()

    referee(userChoice, computerChoice)

    score.printScore()
  }

  enum class Choice {
    Rock, Paper, Scissors
  }

  private fun playerDraw(): Choice {
    print("Hello. Please choose between Rock [r], Paper [p] or Scissors [s]: ")
    val userCommand = readLine()
    val userChoice =
      when (userCommand) {
        "r" -> Choice.Rock
        "p" -> Choice.Paper
        "s" -> Choice.Scissors
        else -> throw RuntimeException("Unknown choice.")
      }
    println("You've chosen $userChoice.")

    return userChoice
  }

  private fun computerDraw(): Choice {
    val choices = Choice.values()
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }

  enum class Winner {
    NoOne, Computer, Player
  }

  private fun referee(userChoice: Choice, computerChoice: Choice) {
    val winner = when (userChoice) {
      Choice.Rock -> rockGiven(computerChoice)
      Choice.Paper -> paperGiven(computerChoice)
      Choice.Scissors -> scissorsGiven(computerChoice)
    }

    val result = when (winner) {
      Winner.NoOne -> "It's a tie."
      Winner.Player -> {
        score.increasePlayerScore()
        "$userChoice beats $computerChoice, the player wins."
      }
      Winner.Computer -> {
        score.increaseComputerScore()
        "$computerChoice beats $userChoice, the computer wins."
      }
    }

    println(result)
  }

  private fun rockGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.NoOne
      Choice.Paper -> Winner.Computer
      Choice.Scissors -> Winner.Player
    }
  }

  private fun paperGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Player
      Choice.Paper -> Winner.NoOne
      Choice.Scissors -> Winner.Computer
    }
  }

  private fun scissorsGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Computer
      Choice.Paper -> Winner.Player
      Choice.Scissors -> Winner.NoOne
    }
  }
}

fun main() {
  val game = Game()

  do {
    game.playARound()

    // One more time?
    print("Would you like to play again? Type 'y' for yes and 'n' for no. ")
    val againResponse = readLine()
  } while (againResponse == "y")
}
```

### Recap

So far we have built a _Rock, Paper, Scissors_ game, where a human player can play against a computer player.
The computer player makes a random choice.
That choice is compared to the choice of the human player.
A winner is declared and the score of the winner is increased by one.

The entry-point function `main()` is kept slim and only contains the main program loop, including the question whether the user would like to play another round.

The `Game` class contains all functionality related to how a single round of the game works.
It hides the inner-workings of the game logic from the `main()` function.

The `Scoreboard` class contains only the functionality related to how the scoreboard knows.
It also hides the inner workings of the scoreboard from the `Game` class.
By doing this, it prevents that the score can be modified in an unintended way.

This recap closes the introductory chapter to the Kotlin syntax.
If you feel like it, here are some ideas to further extend your game:

* Make the program more tolerant against wrong inputs.
  Currently, when an unexpected input is entered by the user, the program crashes.
  You could change the program in a way that it asks the same question again if the input was wrong, until the input matches one of the expected values.
* Make it possible for the player to provide a name.
  Use that name when referring to the player in the game.
* Change the logic of the game in a way, such that the computer always wins.
  Or such that - if the player provides a certain name - the computer never wins.
* Extend the game by adding support for [_Lizard_ and _Spock_][lizard_and_spock], a popular extension to the original _Rock, Paper, Scissors_ game popularized by the tv-show _The Big Bang Theory_.

[lizard_and_spock]: https://bigbangtheory.fandom.com/wiki/Rock,_Paper,_Scissors,_Lizard,_Spock
