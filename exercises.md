---
title: Kotlin/Everywhere Workshop
subtilte: Exercises
author:
  - Christian Mäder
institute:
  - nxt Engineering GmbH
date: 2019-09-10
documentclass: scrreprt
book-class: false # Workaround for https://github.com/jgm/pandoc/issues/5348
papersize: a4
fontfamily: roboto
mainfont: Roboto Regular
toc: true
---

!include chapter2/exercises/index.md

!include chapter3/exercises/index.md
