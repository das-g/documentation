# Translation Tips

This page contains Inkscape tips, translation tips, and more tips to help you while translating Pepper&amp;Carrot SVG files.

#### Table of Content
1. [Translation of Onomatopoeias](#translate-onomatopoeias)
2. [Editing Onomatopoeias in Inkscape](#edit-onomatopoeias)
3. [Speech-bubbles geometry](#speechbubble-geometry)
4. [Bold and Italic](#bold-italic)
5. [Custom episode header](#custom-header)
6. [Custom fonts](#custom-fonts)
7. [Alignement](#alignement)
8. [Language Specific Notes](#specific)

<a name="translate-onomatopoeias"></a>
## 1. Translation of Onomatopoeias

Translating sound effects can be difficult. In case you find yourself having difficulty translating a sound: just trust your instincts provided by comics in your culture and use the spelling that sounds most right to you. Sometime it could be as simple as trying out the sound out loud with your mouth, and then using the phonology of your language. The result is anyway often very subjective and will be only here to show to the reader that a sound is important for the story and/or help their imagination to complete the missing soundtrack. Be creative!

![The horn of episode 33](https://www.peppercarrot.com/data/documentation/medias/img/55a_creative-sound.jpg "One of the most complex soundFX to translate")  

<a name="edit-onomatopoeias"></a>
## 2. Editing Onomatopoeias in Inkscape

I had to find a compromise between having artistic onomatopoeia and something possible to edit. You can always start by editing text as you would do by double clicking on it and type your translation. Many effect will works fine this way. 

Unfortunately, some part of the text effects will be deleted by the editing and not look as it was on the source:

![Common issue](https://www.peppercarrot.com/data/documentation/medias/img/55b_common-issue.jpg "after editing; the style is lost")  

So here is a list of method you can apply to recover the effects:

### Resize letters one by one:

A big classic (and the case in the visual above "common issue") is when the onomatopoeia has different size for each letters. It convey the volume of the sound: fading, or increasing. This type of data are generally lost while editing; but can be edited back by selecting each letter and giving them a different size (eg. 200px, 180px, 160px, etc...).

![](https://www.peppercarrot.com/data/documentation/medias/img/55_letter-size.gif)  

### Rotate letters:

Rotation of individual letters can be very useful to produce cascading effects. I often apply the same value to all the letters of a soundFX and shear the whole box or put the text on a path. Unfortunately, here also the rotation gets crunched at editing the text. Values can be positive (clockwise) or negative (anti-clockwise).

![](https://www.peppercarrot.com/data/documentation/medias/img/55_letter-rotate.gif)  

### Horizontal (in-between character) spacing:

To space the letter or to overlap them. I often use it to overlap them a bit to give a bit more packed aspect to the full sound; making the letter touch each other.

![](https://www.peppercarrot.com/data/documentation/medias/img/55_letter-spacing.gif)  

### Vertical (baseline) spacing:

More rarely used; each letters can have various high compare to the baseline of the text. It's nice to create some waves of high in the text and simulate some modulation of pitch of sound. Some values can be negatives (under the baseline) and some positive (over the baseline).

![](https://www.peppercarrot.com/data/documentation/medias/img/55_letter-baseline.gif)  

### Tweak the aspect (SVG Filters):

Almost all the sounds effect you'll meet in Pepper&Carrot uses SVG filters. These filters are stack of effects that allows to add multiple outlines, variations of stroke, shadows and more.  If you want to tweak the effect and access to this stack of settings, select the text and open the dialog ``Filters > Filter Editor``. Then you'll have sliders to edit all the thickness and colors of the effect. It's rarely required to translate the webcomic; but I can imagine the situation where the effect is too strong and affect the glyph of a language. In this case, calming the turbulance, the width, or the drop shadow of an effect might be useful.

![](https://www.peppercarrot.com/data/documentation/medias/img/55_filter-editor.gif)  

### Advanced: XML editing

To finish, the ultimate method when you want a 1:1 copy of the effect, is to edit the data of the SVG file directly. This method is advanced because it requires to edit the SVG file with a text editor and locate in the jungle of XML tags where the letter of the effect are printed in it. You might need to search (Ctrl+F) for it. But once you found them (often encapsuled in their own XML tags), you can easily replace only the text with your translation, save the text and reopen with Inkscape to see the rendering: the effect should be totally preserved.

![](https://www.peppercarrot.com/data/documentation/medias/img/55_xml.jpg)  



<a name="speechbubble-geometry"></a>
## 3. Speech-bubbles geometry

All speechbubbles are white geometric shapes made of vector point composed of two parts: 

* The body (the bubble)
* The tail

Because both element are white or use the same color and borderless they merge visually into a single shape.

### Padding

Keep a good looking padding distance around the text; a text touching the edge of the speechbubble doesn't look good in general.

### Adapting the Shape

The arrow tool on the top of the vertical toolbar of Inkscape will allow you to deform the edges of the speechbubble. You can adapt the geometry of the speechbubble to match your translation (sometime translation needs larger speechbubble, or smaller). The tool "Tweak Object by Painting or Sculpting" of Inkscape is also useful to deform the speechbubble to wrap better around your texts.

### Mirroring

Sometimes, a speechbubble would fit its text or surroundings more easily if it was mirrored vertically or horizontally, like the bottom speechbubble in this example:

![Example for mirrored speech bubble](https://www.peppercarrot.com/data/documentation/medias/img/flipped-bubble.png "Example for mirrored speech bubble")

This can be a lot faster than manually deforming the speechbubble. Here are the Inkscape buttons to do this:

![Inkscape button for flipping an object](https://www.peppercarrot.com/data/documentation/medias/img/flip-bubble.png "Inkscape button for flipping an object")


<a name="bold-italic"></a>
## 4. Bold and Italic

Select only the text you want to make bold, then change the font style to the bold one. Do the same for Italic.

![](https://www.peppercarrot.com/data/documentation/medias/img/2018-07-01_bold-Lavi-fix.png)

If the font you are using doesn't have a bold variant (for example 'Drukah'), you can select the text and add a StrokePaint to it, and then change the StrokeStyle width. It will create a faux-bold; a method I used a lot on the first episode, before I made the Bold variant for Lavi.

![](https://www.peppercarrot.com/data/documentation/medias/img/2018-02-22_bold-text-faq.jpg)

<a name="custom-header"></a>
## 5. Custom episode header

Tharinda Divakara created a tutorial about how to create a cool vector title with Pepper&Carrot style. You can [read it here](https://www.peppercarrot.com/data/documentation/medias/img/2015-03-04_Sinhala-title-translation_how-to_by-Tharinda-Divakara.jpg "Tutorial about creating vector titles in Pepper&Carrot style").

<a name="custom-fonts"></a>
## 6. Custom fonts

![](https://www.peppercarrot.com/data/documentation/medias/img/2015-03-01_c_install-font.jpg)

You can add [to the Pepper&Carrot 'fonts' Framagit repository](https://framagit.org/peppercarrot/fonts) new fonts for your language. 

### License requirements

The fonts need to be published under a free license or released in the public domain.  
The following font licenses are accepted:

* Public domain fonts
* GNU/GPL fonts
* CC-0 fonts
* CC-BY fonts
* SIL Open Font License (OFL)

### Font information

To ease the storage of the font on the project and its maintenance, it is convenient to also keep the following information in the repository:

* Author name or nickname of the font (File Fontname.COPYRIGHT)
* Link to the source website distributing the font. (File Fontname.COPYRIGHT)
* License file itself, txt version. (File Fontname.LICENSE)

For the formatting, look how it is done for [other fonts in the repository here](https://framagit.org/peppercarrot/fonts/-/tree/master/Latin). 

### Where to find new ones?

Sources for finding new open fonts:

* [Font Library](https://fontlibrary.org/)
* [The League of Moveabletype](https://www.theleagueofmoveabletype.com/)
* [Fontspace](http://www.fontspace.com/category/open) with category “Open”
* [Font Squirrel](https://www.fontsquirrel.com/)


<a name="alignement"></a>
## 7. Alignement

### Center alignment of titles

![English episode header](https://www.peppercarrot.com/data/documentation/medias/img/center-align1.png "Example for English episode header")

On some episode titles and thumbnails, the original text has not been defined as center aligned.
So, since your translation will have a different width, the text layout will not be clean any more:

![Translated episode header](https://www.peppercarrot.com/data/documentation/medias/img/center-align2.png "Example for translated episode header")

If you have this case, simply go to "Object" -> "Align and Distribute...", and in the docker "Align and Distribute..." that should appear (or blink if it was already on your monitor) select the option "Relative to: Page" and click the icon to align the center axis. Done! 

![Center on vertical axis](https://www.peppercarrot.com/data/documentation/medias/img/center-align-end.gif "Center on vertical axis icon")



### Consistent line distance

Sometimes, text will flow across multiple interconnected speechbubbles and we will want to keep the layout consistent. Here's an example:

![English speechbubbles](https://www.peppercarrot.com/data/documentation/medias/img/distribute1.png "Example for English speechbubbles")

If your translation has more or less lines than the original, the layout will not work any more:

![Translated speechbubbles](https://www.peppercarrot.com/data/documentation/medias/img/distribute2.png "Example for translated speechbubbles")

We will want even gaps between the lines (of course, you would also have made the middle bubble less high and moved the text closer together, but we skip this for now).

Use the _Shift_ key and your mouse to select the all text objects that you want to distribute evenly. Then select "Object" -> "Align and Distribute..."

![Align and Distribute menu entry](https://www.peppercarrot.com/data/documentation/medias/img/distribute3.png "Selecting the Align and Distribute entry")


Click on the "Make vertical gaps between objects equal" icon:

![Make vertical gaps between objects equal](https://www.peppercarrot.com/data/documentation/medias/img/distribute4.png "Make vertical gaps between objects equal icon")

The vertical positioning has now been fixed while keeping the horizontal positions as they are.

![Done](https://www.peppercarrot.com/data/documentation/medias/img/done.png "Pepper blowing her wand") Done!


<a name="specific"></a>
## 8. Language Specific Notes

<details>
  <summary>Lojban [jb]: notes about the style</summary>
  <ul>
  <li> always rely on the CLL</li>
  <li> use Simple Lojban since the target audience are children:</li>
  <li> use as few gismu and cmavo as possible</li>
  <li> if a gismu or a cmavo is common then feel free to use it, though</li>
  <li> use any attitudinals/<b>ki'ai</b> + "cmevla/brivla", even experimental ones since the comic is full of interjections/psychomime/etc.</li>
  <li> for translating specific concepts, e.g. names, narrow scientific/math concepts consider using cmevla or (when it's really necessary) experimental brivla (even experimental gismu), e.g. <b>texno</b>, <b>faumji</b>. Such cases must be rare and not break the flow/comprehensibility of episodes</li>
  <li> <b>faumji</b> despite having precise sense can be understood by children as explicitly unfathomable lingo</li>
  <li> <b>texno</b> (if not <b>.texnos.</b>) might be used only for the name of a clan</li>
  <li> spell language/monster language is supposed to be half-comprehensible so consider using distorted Lojban</li>
  </ul>
</details>
<br/>




