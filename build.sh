#!/bin/bash

if [[ "$1" =~ ^(-h|--help)$ ]]; then
  echo "Usage:   $0 [build_targets...]"
  echo "Example: $0 exercises curriculum"
  echo ""
  echo "         build_targets: Which files to build, separated by space."
  echo "                        Default: empty (i.e. build all files)"
  exit 0
fi

MARKDOWN_EXTENSIONS="markdown"
MARKDOWN_EXTENSIONS="${MARKDOWN_EXTENSIONS}+raw_attribute"
MARKDOWN_EXTENSIONS="${MARKDOWN_EXTENSIONS}+backtick_code_blocks"
MARKDOWN_EXTENSIONS="${MARKDOWN_EXTENSIONS}+fenced_code_attributes"
MARKDOWN_EXTENSIONS="${MARKDOWN_EXTENSIONS}+implicit_figures"

PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS-} "
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --pdf-engine=xelatex"
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --standalone"
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --highlight-style=pygments_nxt.theme"
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --filter pandoc-include"
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --filter pandoc-include-code"
PANDOC_DEFAULT_ARGS="${PANDOC_DEFAULT_ARGS} --from ${MARKDOWN_EXTENSIONS}"

PANDOC_ARGS="${PANDOC_ARGS-${PANDOC_DEFAULT_ARGS}}"

DEFAULT_DOCKER_IMAGE="my_pandoc:snapshot"
DOCKER_IMAGE="${DOCKER_IMAGE-${DEFAULT_DOCKER_IMAGE}}"
DOCKER_PANDOC_CMD="docker run --rm -v $(pwd):/pandoc ${DOCKER_IMAGE}"

PANDOC_CMD="${PANDOC_CMD-$DOCKER_PANDOC_CMD}"

PANDOC="${PANDOC_CMD} ${PANDOC_ARGS}"

ALL_ARGUMENTS=( "$@" )
contains() {
  # If empty, return true
  if [ "${#ALL_ARGUMENTS[@]}" == "0" ]; then return 0; fi

  # If value in list, return true
  for i in "${ALL_ARGUMENTS[@]}"; do
    if [ "$i" == "$1" ] ; then
      return 0
    fi
  done

  # Not found, return false
  return 1
}

build_slides() {
  if ! contains "$1"; then
    echo "⏭ Skipping $1"
    return
  fi

  echo "🛠  Generating $1.pdf"

  PANDOC_SLIDES_ARGS="${PANDOC_PAPER_ARGS-} "
  PANDOC_SLIDES_ARGS="$PANDOC_PAPER_ARGS -V aspectratio=169"
  PANDOC_SLIDES_ARGS="$PANDOC_PAPER_ARGS -V theme:focus"

  # shellcheck disable=SC2086
  $PANDOC -t beamer $PANDOC_SLIDES_ARGS -o "$1.pdf" "$1.md"

  echo "✅  Generated $1.pdf"

  echo "🛠  Generating $1.html"
  $PANDOC -t revealjs -s -o "$1.html" "$1.md"

  echo "✅  Generated $1.html"
}

build_paper() {
  if ! contains "$1"; then
    echo "⏭ Skipping $1"
    return
  fi

  echo "🛠  Generating $1.pdf"

  PANDOC_PAPER_ARGS="${PANDOC_PAPER_ARGS-} "
  PANDOC_PAPER_ARGS="$PANDOC_PAPER_ARGS -V colorlinks"
  PANDOC_PAPER_ARGS="$PANDOC_PAPER_ARGS -V links-as-notes"
  PANDOC_PAPER_ARGS="$PANDOC_PAPER_ARGS -V fontfamily=roboto"
  PANDOC_PAPER_ARGS="$PANDOC_PAPER_ARGS -V papersize=a4"

  # shellcheck disable=SC2086
  $PANDOC -t latex $PANDOC_PAPER_ARGS -V "mainfont=Roboto Regular" -o "$1.pdf" "$1.md"

  echo "✅  Generated $1.pdf"
}

build_slides slides
build_paper curriculum
build_paper exercises
