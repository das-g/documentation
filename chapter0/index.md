# Welcome to Workshoptage 2019

## Kotlin/Everywhere

![](chapter0/kotlin_everywhere_logo.png)

This workshop is part of the [_Kotlin/Everywhere_ series][kotlin-everywhere].

[kotlin-everywhere]: https://kotl.in/everywhere

## Timeline

- 15 minutes break at ~10:30
- Lunch break 12:45 and 14:00
- 15 minutes break at ~15:15
- End at 16:15

## Content

- A Brief History of Kotlin
- Kotlin Syntax (and exercises)
- Java and Kotlin Interoperability (and exercise)
