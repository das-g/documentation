# langs.json

The file ``langs.json`` located at the root of the [webcomic repository](https://framagit.org/peppercarrot/webcomics/-/blob/master/langs.json) manages how all the languages are registered in Pepper&Carrot and also document the attribution to the translators.

## Structure

A typical node of this JSON file looks like that:

```
  "cn": {
    "translators": ["Ran Zhuang", "Yan Jing"],
    "name": "Chinese (Simplified)",
    "local_name": "中文",
    "iso_code": "zh",
    "iso_locale": "CN",
    "iso_script": "Hans",
    "iso_version":1
    "font": {
        "family": "Lavi",
        "size": 36,
        "weight": "bold"
    },
    "title_regex": [
        {
            "pattern": "^i ([0-9]+) mo'o ",
            "replacement": "ni'o \\1 mo'o "
        }
    ]
  },
```

**Description:**

``"cn": {``: **The Pepper&Carrot lang ID.** Each node of the json file starts with the two letter that identify the language across the Pepper&Carrot project. It's limited to two characters (so far, two lowercase letters) and you'll find this identifier on the URL of the website, as a directory name for the SVGs in the langs subdirectory of each episode and as a prefix for all rendered JPGs and PNGs files on the server. This identifier is not compliant with any ISO standard but tries to inspire itself from [the ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) two letters whenever it is possible.

``"translators": ["Ran Zhuang", "Yan Jing"],``: **The attribution.** This line contains the name or nickname for each translators who participated on the translation. You can add names adding comas this way ``"translators": ["Translator Name A", "Translator Name B", "Translator Name C"],`` Right now this system is designed to allow external application to correctly attribute translators but this system cannot really transmit an idea of the quantification of work done by a contributor over another one. Unfortunately, a corrector of a single episode will get their name at the same level of the main translator of the language. To write more data, including adding URL, contact information and more background about who done what on the translation, please write it on the [AUTHORS.md](https://framagit.org/peppercarrot/webcomics/-/blob/master/AUTHORS.md) files at the root of the [Webcomic Repository](https://framagit.org/peppercarrot/webcomics/-/tree/master). Limitation: Don't embed HTML tag in this string and if you want to use quotes to write a nickname in the string, you'll need to escape it by placing a backlash in front of it (eg. ``"David \"Deevad\" Revoy"``).

``"name": "Chinese (Simplified)",``: **The English way to name this language.** This line is obvious, if you have no idea, search for the language page on [Wikipedia](https://www.wikipedia.org/) and switch the language selection of the page to English, or look for it in the [Common Locale Data Repository](https://www.unicode.org/cldr/charts/latest/summary/root.html).

``"local_name": "中文",``: **The local way (in the language) to name this language.** This line is obvious too. Here again a search for the language on [Wikipedia](https://www.wikipedia.org/) or the the [Common Locale Data Repository](https://www.unicode.org/cldr/charts/latest/summary/root.html) will give you information about the official name of the language. Note: This text will be reused for the language selector buttons on the top of pages of Pepper&Carrot website.

``"iso_code": "zh",``: **The [the ISO 639](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) language code.** Write in this data socket only official ISO 639 code. It can be ISO 639-1 (preferred), ISO 639-2 or the three letters ISO 639-3. If your language doesn't have any ISO code, leave the socket empty. (eg. ``"iso_code": "",``).

``"iso_locale": "CN",``: **The (optional) [ISO 3166-1](https://en.wikipedia.org/wiki/ISO_3166-1) geographical code.** First, this line is optional: you can remove it if not applicable to your language. This two letters uppercase helps to specify a country, dependent territory, or a special area of geographical interest. Here again a search on the dedicated [Wikipedia ISO 3166-1 table](https://en.wikipedia.org/wiki/ISO_3166-1#Current_codes) will help you to find the appropriate code.

``"iso_script": "Hans",``: **The (optional) [ISO 15924](https://en.wikipedia.org/wiki/ISO_15924) script code.** Only use this if your languages could be written in more than one script, e.g. Chinese (Traditional) vs. Chinese (Simplified), of Serbian (Cyrillic) vs. Serbian (Latin). You can check the [Wikipedia page](https://en.wikipedia.org/wiki/ISO_15924) or the  [Common Locale Data Repository's page](https://www.unicode.org/cldr/charts/latest/supplemental/languages_and_scripts.html) for the correct code.

``"iso_version":1``: **The version of the ISO 639 standard.** This socket of data support only 4 choices: -1, 1, 2 and 3. 
- ``-1`` means the language never was registered in ISO 639-1, ISO 639-2 or ISO 639-3. (eg. Language less commonly spoken as [Gallo language](https://en.wikipedia.org/wiki/Gallo_language) or non registered constructed language). Writing here ``-1`` is mandatory if you left previously the ISO code empty (eg. ``"iso_code": "",``).
- ``1``: The two lowercase letter of [the ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
- ``2``: The two lowercase letter of [the ISO 639-2](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes).
- ``3``: The three lowercase letter of [the ISO 639-3](https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Languages/List_of_ISO_639-3_language_codes_(2019)).

``"font"``: **The font used for rendering the thumbnails in the webcomic page and its size.** Usually this is filled and fine-tuned by David Revoy accordingly to the font you used on translating the title of Pepper&Carrot pages. This information helps the renderfarm to auto-generate the episode thumbnails.
If you have imagemagick installed, you can obtain a list of available fonts in your system by running `convert -list font > fonts.txt` in a terminal.
The `weight` parameter is optional and defaults to `normal`.
For a list of legal weight values see [The Pango Markup Language](https://developer.gnome.org/pygtk/stable/pango-markup-language.html).

``"title_regex"``: **OPTIONAL: Regular expression for transforming title text.** Most languages won't need this parameter at all.
Use it only if your thumbnail episode titles need to differ from the titles on the comic pages.
We introduced this parameter to transform "i 1 mo'o..." to "ni'o 1 mo'o..." etc. for the Lojban language.
If you need rules like this for your language, open an issue and we'll work it out together.


