# Pepper&amp;Carrot Documentation

![](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2016-04-13_carrot-updating-or-repairing_by-David-Revoy.jpg)

### A documentation for:

- **Translators**
- **Developers**
- **Creators of derivations**

The purpose of this wiki is to centralize all the technical information and how-to for you to be independent. I hope you'll find it useful.

**A dynamic and collaborative documentation**

This documentation is dynamic: everyone who has an account on Framagit can propose changes to this wiki by opening a Merge Request on the [Git repository here](https://framagit.org/peppercarrot/documentation). For more information about how to register on Framagit and get developer permissions, read [Translate the comic](https://www.peppercarrot.com/xx/documentation/010_Translate_the_comic.html#framagit).
