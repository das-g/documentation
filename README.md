# Kotlin for Java Developers

This is a training for java developers who would like to learn kotlin.

## Build Locally

Requires Docker!

```bash
docker build -t my_pandoc:snapshot . # Just once, or after changes to Dockerfile
./build.sh
```
