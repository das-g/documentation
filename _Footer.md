**Editing:** David Revoy.   
**Contribution:** GunChleoc.  
**Correction:** Midgard, Sitelen.   
**API documentation:** Jookia.   

![CC-By license image](https://www.peppercarrot.com/data/wiki/medias/img/logo_cc.png)  
[Creative Commons Attribution, CC-By](https://creativecommons.org/licenses/by/4.0/)
